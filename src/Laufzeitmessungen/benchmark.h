#include <iostream>
#include <fstream>
#include <time.h>
#include <chrono>

#include "SearchTree.hpp"
#include "RBTree.hpp"


// Array mit zufallszahlen fuellen:
void fillRandom(size_t *arr, const size_t numValues, size_t& minVal, size_t& maxVal) {
	for(size_t i = 0; i < numValues; i++){   // generierung der zufaelligen zahlen
		arr[i] = rand();

		if(i == 0) {
			minVal = arr[i];
			maxVal = arr[i];
		}

		if(arr[i] < minVal)
			minVal = arr[i];

		if(arr[i] > maxVal)
			maxVal = arr[i];
    }
}


// Array mit durchnummerierten Zahlen fuellen:
void fillNumbered(size_t *arr, const size_t numValues, size_t& minVal, size_t& maxVal) {
	for(size_t i = 0; i < numValues; i++)
		arr[i] = i;
	minVal = 0;
	maxVal = numValues - 1;
}


// Hinfsmethode um laufzeit beliebiger Funktion in nanosekunden zu messen
template<typename Func>
size_t timeNanos(const Func& func) {
	const auto start = std::chrono::high_resolution_clock::now();

	func();

	const auto end = std::chrono::high_resolution_clock::now();
	return std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
}

// Fuehrt einen Benchmark durch und schreibt die Ergebnisse in "Laufzeit_Baum_Ins.csv", "Laufzeit_Baum_Ins_Durch.csv", "Laufzeit_Baum_Ser.csv, "Laufzeit_Baum_Ser_Durch.csv"
void benchmark() {
    srand(time(NULL));

    size_t minElement =       1000 * 1000; // minimale zu testende Anzahl an Elementen
    size_t maxElement = 100 * 1000 * 1000; // maximale zu testende Anzahl an Elementen
    // size_t maxElement = 10 * 1000 * 1000; // maximale zu testende Anzahl an Elementen
    size_t step =             1000 * 1000; // Schritte zur Erhoehung der Anzahl an Elementen

    size_t repeat = 1;
    bool messungBBaum = false;
    bool randomInt = false;
    size_t timeBIns, timeRBIns;
    size_t timeBSer[4]{}, timeRBSer[4]{};

    size_t *valuesArr = new size_t[maxElement];
    size_t valuesMin = 0;
	size_t valuesMax = maxElement - 1;

    std::fstream baumIns, baumInsDurch;
    std::fstream baumSer, baumSerDurch;

    baumIns.open("Laufzeit_Baum_Ins.csv", std::ios::out);
    baumInsDurch.open("Laufzeit_Baum_Ins_Durch.csv", std::ios::out);

    baumSer.open("Laufzeit_Baum_Ser.csv", std::ios::out);
    baumSerDurch.open("Laufzeit_Baum_Ser_Durch.csv", std::ios::out);

    baumIns << "Laufzeiten (in ns)\n";
    baumInsDurch << "Laufzeiten (in ns)\n";

    if(messungBBaum) {
        baumIns << "Elemente;Baum;RB-Baum\n";
        baumInsDurch << "Elemente;Baum;RB-Baum\n";
    }
    else{
        baumIns << "Elemente;RB-Baum\n";
        baumInsDurch << "Elemente;RB-Baum\n";
    }

    baumSer << "Laufzeiten (in ns)\n";
    baumSerDurch << "Laufzeiten (in ns)\n";
    if(messungBBaum) {
        baumSer << "Elemente;Baum;;;;RB-Baum\n";
        baumSer << ";Min;Max;First;Last;Min;Max;First;Last\n";
        baumSerDurch << "Elemente;Baum;;;;RB-Baum\n";
        baumSerDurch << ";Min;Max;First;Last;Min;Max;First;Last\n";
    } else{
        baumSer << "Elemente;RB-Baum\n";
        baumSer << ";Min;Max;First;Last\n";
        baumSerDurch << "Elemente;RB-Baum\n";
        baumSerDurch << ";Min;Max;First;Last\n";
    }

	// Array mit Zufallszahlen fuellen:
	if(randomInt)
    	fillRandom(valuesArr, maxElement, valuesMin, valuesMax);
	else
		fillNumbered(valuesArr, maxElement, valuesMin, valuesMax);


    std::cout << "Anfang der Laufzeitmessungen!\n";

	// Anzahlen an Elementen durchgehen:
    for (size_t i = minElement; i <= maxElement; i += step) {
    	size_t timeBInsDurch = 0;
		size_t timeRBInsDurch = 0;

    	size_t timeBSerDurch[4]{};
		size_t timeRBSerDurch[4]{};

        valuesMax = i;

        for (size_t j = 0; j < repeat; j++) {
            RBTree<int> rbTree1;
            SearchTree<int> tree1;

            if(messungBBaum) {
                //Messung insert Baum
	
				timeBIns = timeNanos([&]() {
					for (size_t k = 0; k < i; k++) {
						tree1.insert(valuesArr[k]);   //Linear
					}
				});
				timeBInsDurch += timeBIns;
            }

            //Messungen insert RB-Baum	
			timeRBIns = timeNanos([&]() {
				for (size_t k = 0; k < i; k++) {
					rbTree1.insert(valuesArr[k]);  //Linear
				}
			});
			timeRBInsDurch += timeRBIns;


            if(messungBBaum) {
                //Messungen Search Baum
                timeBSer[0] = timeNanos([&]() {
                	tree1.search(valuesMin);    //suche minimum
				});

                timeBSer[1] = timeNanos([&]() {
                	tree1.search(valuesMax);    //suche maximum
				});

                timeBSer[2] = timeNanos([&]() {
                	tree1.search(valuesArr[0]);    //suche erstes Element
				});

                timeBSer[3] = timeNanos([&]() {
                	tree1.search(valuesArr[i]);    //suche letztes Element
				});
            }

            //Messungen Search RB-Baum
			timeRBSer[0] = timeNanos([&]() {
            	rbTree1.search(valuesMin);    //suche minimum
			});

			timeRBSer[1] = timeNanos([&]() {
            	rbTree1.search(valuesMax);    //suche maximum
			});

			timeRBSer[2] = timeNanos([&]() {
            	rbTree1.search(valuesArr[0]);    //suche erstes eingefuegtes element
			});

			timeRBSer[3] = timeNanos([&]() {
            	rbTree1.search(valuesArr[i]); // suche letzes eingefuegtes element
			});


            //schreiben in die Dateien
            baumIns << i;
            if(messungBBaum) {
                baumIns << ";" << timeBIns;
            }

            baumIns << ";" << timeRBIns << "\n";

            baumSer << i;
            if(messungBBaum) {
                for (size_t l = 0; l < 4; l++){
                    baumSer << ";" << timeBSer[l];
                    timeBSerDurch[l] += timeBSer[l];
                }
            }
            for (size_t l = 0; l < 4; l++) {
                baumSer << ";" << timeRBSer[l];
                timeRBSerDurch[l] += timeRBSer[l];
            }
            baumSer << "\n";


            std::cout << "Durchlauf " << j << " mit " << i << " Elementen fertig!\n";
        }

        baumInsDurch << i;
        if(messungBBaum) {
            baumInsDurch << ";" << timeBInsDurch / (double)repeat;
        }
        baumInsDurch << ";" << timeRBInsDurch / (double)repeat << "\n";

        baumSerDurch << i;
        if(messungBBaum) {
            for (size_t l = 0; l < 4; l++) {
                baumSerDurch << ";" << timeBSerDurch[l] / (double)repeat;
            }
        }
        for (size_t l = 0; l < 4; l++) {
            baumSerDurch << ";" << timeRBSerDurch[l] / (double)repeat;
        }
        baumSerDurch << "\n";

    }

    baumIns.close();
    baumSer.close();
    baumInsDurch.close();
    baumSerDurch.close();
}
