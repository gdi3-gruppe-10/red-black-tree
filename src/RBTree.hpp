#pragma once

#include <iostream>

// Forward-Declaration, damit RBTreeNode RBTree referenzieren kann:
template<typename T>
class RBTree;

/**
 * Klasse zur Repraesentation eines Rot-Schwarz-Baum-Knotens:
*/
template<typename T>
class RBTreeNode {
	friend class RBTree<T>;

public:
	RBTreeNode* parent = nullptr;
	RBTreeNode* left = nullptr;
	RBTreeNode* right = nullptr;

	enum Color {
		RED,
		BLACK
	} color = BLACK;

public:
	const T key;

public:
	RBTreeNode(const T key = -1) : key(key) {} // Erstellt einen Knoten mit Schluessel <key> (oder defaultwert -1)

	// Disallow (accidental) copying or moving:
	RBTreeNode(const RBTreeNode& copyFrom) = delete;
	RBTreeNode& operator=(const RBTreeNode& copyFrom) = delete;
	RBTreeNode(RBTreeNode&& moveFrom) = delete;
	RBTreeNode& operator=(RBTreeNode&& moveFrom) = delete;

public:
	// optional, aber praktisch zum debuggen:
	friend std::ostream& operator<<(std::ostream& cout, const RBTreeNode* tree) {
		if(tree == nullptr) return cout; // nothing to print

		cout << tree->left << tree->key << ", " << tree->right;

		return cout;
	}

	// inline bool isValid() const {
	// 	if(left != nullptr) {
	// 		if(left->parent != this)
	// 			return false;

	// 		if(!left->isValid())
	// 			return false;
	// 	}

	// 	if(right != nullptr) {
	// 		if(right->parent != this)
	// 			return false;

	// 		if(!right->isValid())
	// 			return false;
	// 	}

	// 	return true;
	// }
};


/**
 * Klasse zur Repraesentation eines Rot-Schwarz-Baumes:
 */
template<typename T>
class RBTree {
	using Node = RBTreeNode<T>; // optional, Fuer uebersichtlichen Code

private:
	Node* nil; // NIL element (existiert immer, selbst bei leerem baum)
	Node* root; // Wurzel (im Falle eines leeren Baumes: nil)

public:
	RBTree(): nil(new Node) { root = nil; } // Erstellt einen neuen Rot-Schwarz-Baum
	virtual ~RBTree() { // Destruktor der Klasse, Iterativ im Stapelueberläufe zu verhindern
		Node* now = root;
		while (now != nil) {
			if (now->left != nil) {
				now = now->left;
			}
			else if (now->right != nil) {
				now = now->right;
			}
			else {
				Node* next = now->parent;
                if (next != nil) {
                    if(next->left == now)
                        next->left = nil;
                    if(next->right == now)
                        next->right = nil;
				}
				delete now;
				now = next;
			}
		}
		delete nil;
	}

	// Disallow (accidental) copying or moving:
	RBTree(const RBTree& copyFrom) = delete;
	RBTree& operator=(const RBTree& copyFrom) = delete;
	RBTree(RBTree&& moveFrom) = delete;
	RBTree& operator=(RBTree&& moveFrom) = delete;

public:
	void insert(const T key); // Fuegt ein Element <key> in den Baum ein
	Node* search(const T key); // sucht nach Knoten mit Schluessel <key>

private: // Private interne Methoden:
	void rotateLeft(Node* x);
	void rotateRight(Node* x);
	void insertFixup(Node* z);


public:
	// optional, aber praktisch zum debuggen:
	friend std::ostream& operator<<(std::ostream& cout, const RBTree& tree) {
		const auto printSub = [&](const auto& printSub, const Node* node){
			if(node == tree.nil) return;
			cout << "(";
			printSub(printSub, node->left);
			// cout << " <- ";
			cout << " ";
			cout << node->key;
			cout << " ";
			// cout << " -> ";
			printSub(printSub, node->right);
			cout << ")";
		};

		printSub(printSub, tree.root); // markiert rootNode
		return cout;
	}

	// preorder-print:
	void printSubtree(const Node* subTree, const size_t depth) const {
		if(subTree == nil) return;

		for(size_t i = 0; i < depth; i++)
			std::cout << "  ";
		std::cout << subTree->key << "\n";

		printSubtree(subTree->right, depth + 1);

		printSubtree(subTree->left, depth + 1);
	}

	// preorder-print:
	void print() const {
		printSubtree(root, 0);
	}

	// bool isValid() const {
	// 	if(root == nullptr)
	// 		return true;

	// 	if(root->parent != nullptr)
	// 		return false;

	// 	return root->isValid();
	// }

	// gibt true zurueck falls Baum leer ist:
	bool isEmpty() const {
		return root == nil;
	}
};

// Implementierungen der Funktionen koennen in separate Datei ausgelagert werden:
#include "RBTree_impl.hpp"